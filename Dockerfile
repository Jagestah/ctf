FROM kalilinux/kali-rolling:latest

RUN apt-get -y update && apt-get clean \
    && apt-get install -y --no-install-recommends \
    kali-tools-top10 \
    kali-linux-headless
        # https://www.kali.org/tools/aircrack-ng
        # https://www.kali.org/tools/burpsuite
        # https://www.kali.org/tools/crackmapexec
        # https://www.kali.org/tools/hydra
        # https://www.kali.org/tools/john
        # https://www.kali.org/tools/metasploit-framework
        # https://www.kali.org/tools/nmap
        # https://www.kali.org/tools/responder
        # https://www.kali.org/tools/sqlmap
        # https://www.kali.org/tools/wireshark

    # ffuf \
    #     # https://www.kali.org/tools/ffuf/
    # kismet \
    #     # https://www.kali.org/tools/kismet/
    # dnsrecon \
    #     # https://www.kali.org/tools/dnsrecon/
    # dnsenum \
    #     # https://www.kali.org/tools/dnsenum/
    # yersinia \
    #     # https://www.kali.org/tools/yersinia/
    # fcrackzip \
    #     # https://www.kali.org/tools/fcrackzip/