# ctf

## Coordination
### Goals
- Don't step on each other's toes
- Share information as it becomes available
- Reduce siloes when working challenges that are dependent on information gathered from other challenges

### Implementation
- A dedicated google drive that everyone on the team is granted access to

## Gameplan
- Enumerate DNS
- nmap found subdomains
    - nmap can also run scripts to check for common vulnerabilities when a port is found open
- fuzz found subdomains
    - Great for identifying path traversal
- Gather all images and run them through a couple [stenogagraph](https://www.kaspersky.com/resource-center/definitions/what-is-steganography) tools
    - https://stylesuxx.github.io/steganography/
- Keep an eye out for several different potential iterations of the flag
    - Plain text
    - base64 encoded
    - Reversed and base64 encoded
- grep all the things
    - Use `-r` to grep recursively
    - Use `-a` to grap binaries as if they were strings
- Run `john the ripper` on everything that looks like a password hash
    - Especially on the `/etc/passwd` and `/etc/shadow` files
    - Probably stick with mostly dictionary attacks
- If a challenge comes with a private network
    - packet capture the network
        - Look for DNS requests that may indicate users accessing incorrect domains or typos
        - Look for any unencrypted HTTP traffic
        - Look for SERVER/CLIENT HELLO packets that might include certificate transmission

## TODO
- Notes on SQL injection
- Notes on cryptographic cyphers
- Notes IoT hacking
- Kali in a container with a GUI



## Tools to Install
- `dnsrecon`
- `john the ripper`
- `nmap`
- `wireshark`